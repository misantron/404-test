<?php

namespace App\Providers;

use App\Services\Messenger\ClientFactory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        //
    }

    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(ClientFactory::class, function () {
            return new ClientFactory();
        });
    }
}
