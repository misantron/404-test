<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MessageLog
 * @package App
 *
 * @property int $id
 * @property string $messenger
 * @property string $recipient_id
 * @property string $body
 * @property string $body_hash
 * @property Carbon|null $send_at
 * @property string|null $error
 */
class MessageLog extends Model
{
    protected $table = 'message_logs';

    protected $dateFormat = 'Y-m-d H:i';

    protected $fillable = [
        'messenger',
        'recipient_id',
        'body',
        'body_hash',
        'send_at',
        'error',
    ];

    protected $dates = [
        'send_at'
    ];
}
