<?php

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class Message
 * @package App\DTO
 */
class Message extends DataTransferObject
{
    /** @var string */
    public $body;

    /**
     * @var array
     */
    public $recipients;

    /**
     * @var string|null
     */
    public $send_at;
}