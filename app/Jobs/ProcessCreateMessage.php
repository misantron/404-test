<?php

namespace App\Jobs;

use App\DTO\Message;
use App\MessageLog;
use App\Services\Messenger\ClientFactory;

/**
 * Class ProcessCreateMessage
 * @package App\Jobs
 */
class ProcessCreateMessage extends Job
{
    /**
     * Максимальное количество повторений при обработке задачи
     *
     * @var int
     */
    public $tries = 5;

    /**
     * @var Message
     */
    private $message;

    /**
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }

    /**
     * @param ClientFactory $clientFactory
     */
    public function handle(ClientFactory $clientFactory): void
    {
        $bodyHash = sha1($this->message->body);

        // предварительно загружаем список получателей с таким же телом сообщения
        $forbiddenRecipients = MessageLog::query()
            ->where('body_hash', $bodyHash)
            ->get()
            ->pluck('recipient_id')
            ->all();

        $logs = [];

        foreach ($this->message->recipients as $row) {

            $client = $clientFactory->get($row['messenger']);

            foreach ($row['ids'] as $id) {

                // игнорируем отправку для получателей, которым сообщение уже отправлялось
                if (in_array($id, $forbiddenRecipients, true)) {
                    continue;
                }

                $error = null;

                try {
                    $client->send($id, $this->message->body);
                } catch (\Throwable $e) {
                    // ловим ошибки отправки для сохранения в лог
                    $error = $e->getMessage();
                }

                // формируем запись лога для сохранения
                $logs[] = [
                    'body' => $this->message->body,
                    'body_hash' => $bodyHash,
                    'recipient_id' => $id,
                    'messenger' => $row['messenger'],
                    'send_at' => $this->message->send_at,
                    'error' => $error,
                ];
            }
        }

        // сохраняем лог отправки сообщения в БД
        MessageLog::query()->insert($logs);
    }
}
