<?php

namespace App\Services\Messenger;

use GuzzleHttp\Exception\RequestException;

/**
 * Interface ClientInterface
 * @package App\Services\Messenger
 */
interface ClientInterface
{
    /**
     * @param string $recipient
     * @param string $message
     *
     * @throws RequestException
     */
    public function send(string $recipient, string $message): void;
}