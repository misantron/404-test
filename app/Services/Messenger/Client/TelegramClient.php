<?php

namespace App\Services\Messenger\Client;

use App\Services\Messenger\ClientInterface;

/**
 * Class TelegramClient
 * @package App\Services\Messenger\Client
 */
class TelegramClient implements ClientInterface
{
    public const NAME = 'telegram';

    /**
     * @param string $recipient
     * @param string $message
     */
    public function send(string $recipient, string $message): void
    {
        // TODO: Implement send() method.
    }
}