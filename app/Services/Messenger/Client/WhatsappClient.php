<?php

namespace App\Services\Messenger\Client;

use App\Services\Messenger\ClientInterface;

/**
 * Class WhatsappClient
 * @package App\Services\Messenger\Client
 */
class WhatsappClient implements ClientInterface
{
    public const NAME = 'whatsapp';

    /**
     * @param string $recipient
     * @param string $message
     */
    public function send(string $recipient, string $message): void
    {
        // TODO: Implement send() method.
    }
}