<?php

namespace App\Services\Messenger\Client;

use App\Services\Messenger\ClientInterface;

/**
 * Class ViberClient
 * @package App\Services\Messenger\Client
 */
class ViberClient implements ClientInterface
{
    public const NAME = 'viber';

    /**
     * @param string $recipient
     * @param string $message
     */
    public function send(string $recipient, string $message): void
    {
        // TODO: Implement send() method.
    }
}