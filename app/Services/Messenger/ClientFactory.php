<?php

namespace App\Services\Messenger;

/**
 * Class ClientFactory
 * @package App\Services\Messenger
 */
class ClientFactory
{
    /**
     * @var array
     */
    protected static $cache = [];

    /**
     * @param string $messenger
     * @return ClientInterface
     */
    public function get(string $messenger): ClientInterface
    {
        if (!isset(static::$cache[$messenger])) {
            static::$cache[$messenger] = $this->createClient($messenger);
        }

        return static::$cache[$messenger];
    }

    /**
     * @param string $messenger
     * @return ClientInterface
     */
    private function createClient(string $messenger): ClientInterface
    {
        $clientClassName = ucfirst($messenger) . 'Client';
        $clientClassPath = __NAMESPACE__ . '\\Client\\' . $clientClassName;

        if (!class_exists($clientClassPath)) {
            throw new \InvalidArgumentException('Unexpected messenger name provided');
        }

        return new $clientClassPath();
    }
}