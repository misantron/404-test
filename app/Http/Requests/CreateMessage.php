<?php

namespace App\Http\Requests;

use App\DTO\Message;
use App\Services\Messenger\Client\TelegramClient;
use App\Services\Messenger\Client\ViberClient;
use App\Services\Messenger\Client\WhatsappClient;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateMessage
 * @package App\Http\Requests
 */
class CreateMessage extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'body' => [
                'required',
                'string',
                'max:10000',
            ],
            'recipients' => [
                'required',
                'array',
            ],
            'recipients.*.messenger' => [
                'required',
                'string',
                Rule::in([
                    TelegramClient::NAME,
                    ViberClient::NAME,
                    WhatsappClient::NAME,
                ]),
            ],
            'recipients.*.ids' => [
                'required',
                'array',
            ],
            'send_at' => [
                'nullable',
                function ($attribute, $value, $fail) {
                    if ($value !== null && Carbon::parse($value)->lessThan(Carbon::now())) {
                        $fail("{$attribute} must be a future date");
                    }
                }
            ],
        ];
    }

    /**
     * @return Message
     */
    public function validated(): Message
    {
        $data = parent::validated();

        return new Message($data);
    }
}
