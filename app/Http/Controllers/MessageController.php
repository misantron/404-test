<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMessage;
use App\Jobs\ProcessCreateMessage;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

/**
 * Class MessageController
 * @package App\Http\Controllers
 */
class MessageController extends Controller
{
    /**
     * @param CreateMessage $request
     * @return JsonResponse
     */
    public function create(CreateMessage $request): JsonResponse
    {
        $message = $request->validated();

        if ($message->send_at === null) {
            ProcessCreateMessage::dispatch($message)->onQueue('messages');
        } else {
            // если в запросе было передано время отправки - добавляем задержку при создании job
            ProcessCreateMessage::dispatch($message)
                ->onQueue('messages')
                ->delay(Carbon::parse($message->send_at));
        }

        return response()->json(['success' => true]);
    }
}