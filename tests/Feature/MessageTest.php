<?php

namespace Tests\Feature;

use App\Jobs\ProcessCreateMessage;
use App\MessageLog;
use App\Services\Messenger\Client\TelegramClient;
use App\Services\Messenger\Client\ViberClient;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class MessageTest extends TestCase
{
    public function testCreateWithInvalidRequest()
    {
        $request = [
            'body' => 'Hello world',
            'recipients' => [
                [
                    'messenger' => 'invalid',
                    'ids' => [mt_rand(1, 1000)],
                ],
            ],
        ];

        $this
            ->actingAs($this->user, 'api')
            ->postJson('/api/message', $request, [
                'X-Requested-With' => 'XMLHttpRequest',
            ])
            ->assertStatus(422);
    }

    public function testCreate()
    {
        $request = [
            'body' => 'Hello world',
            'recipients' => [
                [
                    'messenger' => TelegramClient::NAME,
                    'ids' => [mt_rand(1, 1000)],
                ],
            ],
        ];

        Queue::fake();

        $this
            ->actingAs($this->user, 'api')
            ->postJson('/api/message', $request)
            ->assertOk();

        Queue::assertPushedOn(
            'messages',
            ProcessCreateMessage::class,
            function (ProcessCreateMessage $job) use ($request) {
                return $request['body'] === $job->getMessage()->body;
            }
        );
    }

    public function testCreateWithSameBody()
    {
        $request = [
            'body' => 'Hello world',
            'recipients' => [
                [
                    'messenger' => TelegramClient::NAME,
                    'ids' => ['123456'],
                ],
            ],
        ];

        $this
            ->actingAs($this->user, 'api')
            ->postJson('/api/message', $request)
            ->assertOk();

        $this->assertSame(1, MessageLog::query()->count());

        $request = [
            'body' => 'Hello world',
            'recipients' => [
                [
                    'messenger' => TelegramClient::NAME,
                    'ids' => ['123456'],
                ],
            ],
        ];

        $this
            ->actingAs($this->user, 'api')
            ->postJson('/api/message', $request)
            ->assertOk();

        $this->assertSame(1, MessageLog::query()->count());
    }

    public function testCreateDelayed()
    {
        $request = [
            'body' => 'Hello world',
            'recipients' => [
                [
                    'messenger' => TelegramClient::NAME,
                    'ids' => [mt_rand(1, 1000)],
                ],
            ],
            'send_at' => Carbon::now()->addDays(2)->format('Y-m-d H:i'),
        ];

        $this
            ->actingAs($this->user, 'api')
            ->postJson('/api/message', $request)
            ->assertOk();

        /** @var MessageLog $log */
        $log = MessageLog::query()->firstOrFail();

        $this->assertSame(TelegramClient::NAME, $log->messenger);
        $this->assertSame('Hello world', $log->body);
        $this->assertSame($request['send_at'], $log->send_at->format('Y-m-d H:i'));
        $this->assertNull($log->error);
    }

    public function testCreateBatch()
    {
        $request = [
            'body' => 'Hello world',
            'recipients' => [
                [
                    'messenger' => TelegramClient::NAME,
                    'ids' => [mt_rand(1, 1000), mt_rand(1, 1000)],
                ],
                [
                    'messenger' => ViberClient::NAME,
                    'ids' => [mt_rand(1, 1000)],
                ]
            ],
        ];

        $this
            ->actingAs($this->user, 'api')
            ->postJson('/api/message', $request)
            ->assertOk();

        $this->assertSame(3, MessageLog::query()->count());
    }
}