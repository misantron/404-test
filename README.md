# Тестовое задание

## Ресурс /api/message

Метод: `POST`

Формат запроса:

```
{
    "body": "string|max:10000",
    "recipients": [
        {
            "messenger": "telegram",
            "ids": ["4266472", "6627427"]
        },
        {
            "messenger": "viber",
            "ids": ["74724842"]
        }
    ],
    "send_at": "2019-03-07 15:40"
}
```

## Запуск тестов

``vendor/bin/phpunit``

